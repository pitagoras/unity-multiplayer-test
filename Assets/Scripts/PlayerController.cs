﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class PlayerController : NetworkBehaviour {

	public GameObject bulletPrefab;
	public Transform bulletSpawn;
	public GameObject playerCam;
	public PlayerTeam team;
	
	// Use this for initialization
	void Start () {
	
	}
	
	void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 150.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 3.0f;
		transform.position = new Vector3 (transform.position.x, 0, transform.position.z);

        transform.Rotate(0, x, 0);
        transform.Translate(0, 0, z);
		
		if(Input.GetKeyDown(KeyCode.Space)){
			CmdFire();
		}
    }
	
	[Command]
	protected void CmdFire(){
		GameObject bullet = (GameObject) Instantiate(bulletPrefab,
													bulletSpawn.position, 
													bulletSpawn.rotation);
		
		bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * 6.0f;
		
		NetworkServer.Spawn(bullet);
		
		Destroy(bullet, 2);
		
	}
	
	//runs only in the local player
	public override void OnStartLocalPlayer(){
		//base.OnStartLocalPlayer();
		GetComponent<MeshRenderer>().material.color = Color.blue;
		playerCam.SetActive(true);
		this.team = GlobalData.team;
		transform.position = FindObjectOfType<TeamSpawner>().getPosition (team);
	}
}
