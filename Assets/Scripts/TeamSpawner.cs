﻿using UnityEngine;
using System.Collections;

public class TeamSpawner : MonoBehaviour {

	public Transform redTeamPos, blueTeamPos;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Vector3 getPosition(PlayerTeam team){
		if (team == PlayerTeam.RED) {
			return redTeamPos.position;
		} else /*if (team == PlayerTeam.BLUE)*/ {
			return blueTeamPos.position;
		}
	}
}
