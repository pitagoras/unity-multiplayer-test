﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LobbyCanvas : MonoBehaviour {
	public Image panelImage;
	public Text teamLabel;
	Color red, blue;

	// Use this for initialization
	void Start () {
		float alpha = 0.5f;
		red = Color.red;
		red.a = alpha;
		blue = Color.blue;
		blue.a = alpha;
	}
	
	public void onChangeTeam(){
		if (GlobalData.team == PlayerTeam.BLUE) {
			panelImage.color = red;
			teamLabel.text = "Time Vermelho";
			GlobalData.team = PlayerTeam.RED;
		} else if (GlobalData.team == PlayerTeam.RED){
			panelImage.color = blue;
			teamLabel.text = "Time Azul";
			GlobalData.team = PlayerTeam.BLUE;
		}
	}
}
