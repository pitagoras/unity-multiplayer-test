﻿using UnityEngine;
using System.Collections;

public class DamageOnCollisionEnter : MonoBehaviour {
	public int damage = 10;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnCollisionEnter(Collision collision){
		GameObject hit = collision.gameObject;
		Health health = hit.GetComponent<Health>();
		if(health != null){
			health.TakeDamage(damage);
		}
	}
}
