﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health : NetworkBehaviour
{
	public bool isPlayer;
	public const int maxHealth = 100;
	[SyncVar (hook = "onChangeHealth")] public int currentHealth = maxHealth;
	public RectTransform healthBar;
	private NetworkStartPosition[] spawnPoints;

	void Start(){
		if (isLocalPlayer) {
			spawnPoints = FindObjectsOfType<NetworkStartPosition> ();
		}
	}

	public void TakeDamage (int amount)
	{
		if (!isServer) {
			return;
		}
		currentHealth -= amount;
		if (currentHealth <= 0) {
			currentHealth = maxHealth;
			RpcRespawn ();
		}

	}

	void onChangeHealth (int health)
	{
		healthBar.sizeDelta = new Vector2 (health * 2, healthBar.sizeDelta.y);
	}

	[ClientRpc]
	void RpcRespawn ()
	{
		if (isLocalPlayer || !isPlayer) {	
			Vector3 spawnPoint = Vector3.zero;

			if (spawnPoints != null && spawnPoints.Length > 0) {
				spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length-1)].transform.position;
			}
			transform.position = spawnPoint;
		}
	}
}
